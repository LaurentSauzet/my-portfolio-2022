import React from "react";
import { motion } from "framer-motion";

type Props = {};

export default function ExperienceCard({}: Props) {
  return (
    <article className="flex flex-col rounded-lg items-center space-y-7 flex-shrink-0 w-[500px] md:w-[600px] xl:w-[900px] snap-center bg-[#2d2d54] p-10 hover:opacity-100 opacity-40 cursor-pointer transition-opacity duration-200 overflow-hidden">
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{
          opacity: 1,
          y: 0,
        }}
        viewport={{
          once: true,
        }}
        className="w-32 h-32 rounded-full xl:w-[200px] xl:h-[200px] object-contain object-center bg-white aspect-{auto}"
        src="https://la-colloc.co/wp-content/uploads/2018/10/la-colloc-logo.png"
        alt="Logo La Colloc"
      />

      <div className="px-0 md:px-10">
        <h4 className="text-4xl font-light">Développeur Fullstack</h4>
        <p className="font-bold text-2xl mt-1">La Colloc</p>
        <div className="flex space-x-2 my-2">
            {/* VueJS2 */}
            <img 
            className="h-10 w-10 rounded-full object-cover"
            src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/08/VueJS.jpg" alt="Logo de VueJS" />
            {/* VueRouter */}
            <img 
            className="h-10 w-10 rounded-full object-cover"
            src="https://madewithnetworkfra.fra1.digitaloceanspaces.com/spatie-space-production/23798/vue-router.jpg" alt="Logo de VueJS" />
            {/* VueX */}
            <img 
            className="h-10 w-10 rounded-full bg-white"
            src="https://dyma.fr/assets/technos/vuex.png" alt="Logo de VueJS" />
            {/* Axios */}
            <img 
            className="h-10 w-10 rounded-full"
            src="https://play-lh.googleusercontent.com/_ATfgR5IQv2JcYauNzhTgntADBECazjfAkHMmq9xDj2Mcwts18TEJ9m3SYUNtdbsxog" alt="Logo de VueJS" />
            {/* Node */}
            <img 
            className="h-10 w-10 rounded-full object-cover"
            src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/07/NodeJS.jpg" alt="Logo de VueJS" />
            {/* mongo */}
            <img 
            className="h-10 w-10 rounded-full"
            src="https://g.foolcdn.com/art/companylogos/square/mdb.png" alt="Logo de VueJS" />
            {/* laravel */}
            <img 
            className="h-10 w-10 rounded-full bg-white object-contain"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png" alt="Logo de VueJS" />
            {/* mysql */}
            <img 
            className="h-10 w-10 rounded-full bg-white"
            src="https://cdn2.boryl.fr/2020/12/fd8a25a0-mysql.svg" alt="Logo de VueJS" />
            {/* docker */}
            <img 
            className="h-10 w-10 rounded-full object-cover"
            src="https://grafikart.fr/uploads/attachments/2015/634-600a91d4e1c7b865193005.jpg" alt="Logo de VueJS" />
            {/* gitlab */}
            <img 
            className="h-10 w-10 rounded-full bg-white"
            src="https://humancoders-formations.s3.amazonaws.com/uploads/course/logo/155/formation-gitlab.png" alt="Logo de VueJS" />
        </div>
        <p className="uppercase py-5 text-[#6667AB]">Started work... Ended...</p>
        <ul className="list-disc space-y-4 ml-5 text-lg ">
          <li>Summary points</li>
          <li>Summary points</li>
          <li>Summary points</li>
          <li>Summary points</li>
          <li>Summary points</li>
        </ul>
      </div>
    </article>
  );
}
