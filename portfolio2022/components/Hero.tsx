import React from "react";
import { Cursor, useTypewriter } from "react-simple-typewriter";
import BackgroundCircles from "./BackgroundCircles";
import Link from "next/link"

type Props = {};

export default function Header({}: Props) {
  const [text, count] = useTypewriter({
    words: [
      "Bonjour, je suis Laurent Sauzet",
      "<Developer />",
      "@Designer",
      "<Creator />",
    ],
    loop: true,
    delaySpeed: 2000,
  });
  return (
    <div className="h-screen flex flex-col space-y-8 items-center justify-center text-center overflow-hidden">
      <BackgroundCircles></BackgroundCircles>
      <img
        className="relative rounded-full h-32 w-32 mx-auto object-cover"
        src="https://pbs.twimg.com/profile_images/1524763579769098243/bY8KqU1d_400x400.jpg"
        alt="Photo de Laurent Sauzet"
      />
      <div className="z-20">
        <h2 className="text-sm uppercase text-[#7274e5] pb-2 tracking-[15px]">
          Développeur web
        </h2>
        <h1>
          <span className="text-5xl lg:text-6xl font-semibold px-10">
            {text}
          </span>
          <Cursor cursorColor="#f7ab0a" />
        </h1>

        <div className="pt-5">
          <Link href="#About">
            <button className="heroButton">About</button>
          </Link>
          <Link href="#Experience">
            <button className="heroButton">Experience</button>
          </Link>
          <Link href="#Skills">
            <button className="heroButton">Skills</button>
          </Link>
          <Link href="#Projects">
            <button className="heroButton">Projects</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
